FROM openjdk:8-jdk-oraclelinux7
VOLUME /tmp
WORKDIR /app
COPY ./target/*.jar /app/app.jar
RUN jar -xf /app/app.jar
RUN rm -rf app.jar
ENTRYPOINT ["java","-cp","BOOT-INF/classes:BOOT-INF/lib/*","id.co.gtx.sacservicezuul.SacServiceZuulApplication"]