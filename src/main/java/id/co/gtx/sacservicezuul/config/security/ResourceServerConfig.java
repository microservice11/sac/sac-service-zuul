package id.co.gtx.sacservicezuul.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    private static final String RESOURCE_ID = "inventory";

    @Value("${security.oauth2.client.client-id}")
    private String clientId;

    @Value("${security.oauth2.client.client-secret}")
    private String clientSecret;

    @Value("${security.oauth2.resource.token-info-uri}")
    private String tokenInfoUri;

    @Autowired
    private TokenStore tokenStore;

    @Bean
    public RemoteTokenServices tokenServices() {
        RemoteTokenServices tokenServices = new RemoteTokenServices();
        tokenServices.setClientId(clientId);
        tokenServices.setClientSecret(clientSecret);
        tokenServices.setCheckTokenEndpointUrl(tokenInfoUri);
        return tokenServices;
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources
                .resourceId(RESOURCE_ID)
                .tokenStore(tokenStore)
                .tokenServices(tokenServices());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/uaa/auth/**",
                        "/uaa/oauth/**",
                        "/uaa/data/**",
                        "/uaa/v2/**",
                        "/inventory/v2/**",
                        "/config/v2/**",
                        "/log/v2/**",
                        "/server/swagger/**").permitAll()
                .antMatchers("/server/**").denyAll()
                .antMatchers(HttpMethod.GET, "/inventory/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_OPERATOR", "ROLE_HD")
                .antMatchers("/config/check/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_OPERATOR", "ROLE_HD")
                .antMatchers("/config/create").hasAnyAuthority("ROLE_ADMIN", "ROLE_OPERATOR")
                .antMatchers(HttpMethod.GET, "/data/users/username/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_OPERATOR", "ROLE_HD")
                .antMatchers(HttpMethod.GET, "/data/users/id/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_OPERATOR", "ROLE_HD")
                .antMatchers(HttpMethod.PUT, "/data/users/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_OPERATOR", "ROLE_HD")
                .antMatchers("/inventory/**", "/data/**").hasAnyAuthority("ROLE_ADMIN")
                .anyRequest().authenticated();
    }
}
