package id.co.gtx.sacservicezuul.config.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Primary
@Configuration
@EnableSwagger2
public class SwaggerConfig implements SwaggerResourcesProvider {

    @Bean
    UiConfiguration uiConfig() {
        return new UiConfiguration("validatorUrl", "list", "alpha", "schema",
                UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS, false, true, 60000L);
    }

    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();
        resources.add(resource("sac-service-client", "/config/v2/api-docs", "2.0"));
        resources.add(resource("sac-service-inventory", "/inventory/v2/api-docs", "2.0"));
        resources.add(resource("sac-service-log", "/log/v2/api-docs", "2.0"));
        resources.add(resource("sac-service-security", "/uaa/v2/api-docs", "2.0"));
        resources.add(resource("sac-service-server", "/server/swagger/?format=openapi", "2.0"));
        return resources;
    }

    private SwaggerResource resource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
